# Prácticas en Rust
Aquí almaceno las prácticas que he hecho basadas en el libro oficial para aprender Rust, [The Rust Programming Language](https://doc.rust-lang.org/book/) con el principal (pero no exclusivo) objetivo de **poder ver mi progreso a futuro**.

## IMPORTANTE
* No hago gran cosa aquí. Por favor no esperes algoritmos o un gran uso de Rust idiomático (por ahora).
* *Lo anterior no significa que experimente con cosas más raras*. En algún punto las subiré.
* Se puede dar el caso de que algunas prácticas fallen. En lo que vaya mejorando iré solucionando, *pero no doy garantías*.
* Si eres nuevo en Rust algunas de mis prácticas te podrían ayudar: Hay algunos códigos que he "documentado" más profundamente.
* Si ves que hago algo mal, por favor házmelo saber c:
