#![allow(unused_variables)]

fn main() {
    use std::collections::HashMap;

    //Creación I:
    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);

    //Creación II:
    let teams = vec![String::from("Blue"), String::from("Yellow")];
    let initial_scores = vec![10, 50];
    let scores: HashMap<_, _> = teams.iter().zip(initial_scores.iter()).collect();
    //iter(): Iterador del vector.
    //zip(): Crea vector de tuplas; valores consecutivos: (teams, initial_scores).
    //colect(): Toma el vector de tuplas y lo transforma en una colección.

    //Acceso a valores:
    print!("Acceso único: ");
    let team_name = String::from("Blue");
    let score = scores.get(&team_name);
    if let Some(&i) = score {
        println!("{}", &i);
    }

    //Iterando:
    println!("Iterando en un Hash Map");
    for (key, value) in &scores {
        println!("{}: {}", key, value);
    }

    //Actualizando:
    //1: Sobreescritura de valor.
    //2: Actualización en caso de que key no tenga valor.
    //3: Actualización basada en el valor anterior.

    print!("Sobreescritura de valor: ");
    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10); //No permanece.
    scores.insert(String::from("Blue"), 25);
    println!("{:?}", scores);

    println!("Actualización en caso de que key no tenga valor:");
    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);
    scores.entry(String::from("Yellow")).or_insert(50); //Se agrega.
    scores.entry(String::from("Blue")).or_insert(50); //No cambia.
    println!("{:?}", scores);

    println!("Actualización basada en el valor anterior");
    let text = "hello world wonderful world";
    let mut map = HashMap::new();
    for word in text.split_whitespace() {
        let count = map.entry(word).or_insert(0);
        *count += 1;
    }
    println!("{:?}", map);

    //Ownership:
    let field_name = String::from("Favorite color");
    let field_value = String::from("Blue");
    let mut map = HashMap::new();
    map.insert(field_name, field_value);
    //field_name y field_value han sido movidos a map.
    //ATENCIÓN:
    //map.insert(&field_name, &field_value);
    //No ocasiona cambio de ownership
    //Sin embargo involucra uso de lifetimes.
    //Esto para mantener válidas las referencias.
}
