#![allow(unused_variables)]

fn main() {
    let hello = "Hola";
    let s = hello.to_string();

    let hello = String::from("שָׁלוֹם");
    let hello = String::from("Здравствуйте");
    
    //Añadiendo elementos:
    let mut s = String::new();
    s.push('f');
    s.push_str("oobar");
    println!("{}", s);
    let text = " some";
    s.push_str(text);
    println!("{}", text);
    println!("{}", s);

    //Concatenación:
    //Operador + obtiene ownership del argumento izquierdo.
    //Macro format! no obtiene ownership.
    let s1 = String::from("Hello ");
    let s2 = String::from("world!");
    let s3 = s1 + &s2; //s1 es movido acá.
    println!("{}", s3);

    let s1 = String::from("tic");
    let s2 = String::from("tac");
    let s3 = String::from("toe");
    //let s = s1 + "-" + &s2 + "-" + &s3;
    let s = format!("{}-{}-{}", s1, s2, s3);
    println!("{}", s);
    println!("{}", s1); //s1 no es movido.

    //Representación interna de Strings:
    //Representación: Unicode UTF-8.
    let s = String::from("Hola");
    let len = s.len();
    println!("len: {}", len); //Salida: 4. 1 byte por caracter.
    let s = String::from("Здравствуйте");
    let len = s.len();
    println!("len: {}", len); //Salida: 24. 2 bytes por caracter.

    //Tres tipos de representación de strings:
    //1: Por bytes: Bytes reales en memoria.
    //2: Por los valores escalares Unicode: Representación por tipos char.
    //3: Por clúster graphemes: Representación por letras.
    
    //Indexación:
    let s = String::from("Indéxame.");
    //Los strings no se indexan por posición debido a que en dicha
    //posición puede estar PARTE de un caracter.
    //let index = &s[0]; //No funciona.

    let hello = "Здравствуйте";
    let s = &hello[0..4];
    //let s = &hello[0..1]; //Panic porque está representado de a dos bytes.
    println!("{}", s);

    //Iterando sobre strings:
    println!("Valores escalares de Unicode:");
    for c in "नमस्ते".chars() {
        print!("{} ", c);
    }
    print!("\n");

    println!("Bytes de un string:");
    let mut contador = 0;
    for c in "नमस्ते".bytes() {
        print!("{} ", c);
        contador += 1;
    }
    println!("\nBytes totales: {}", contador);
}
