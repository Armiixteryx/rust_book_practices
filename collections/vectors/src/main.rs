#![allow(unused_variables)]
fn main() {
    //Declaraciones.
    let v: Vec<i32> = Vec::new();
    let v = vec![1, 2, 3];

    //Agregando elementos.
    let mut v = Vec::new();
    v.push(4);
    v.push(5);
    v.push(6);

    {
        let v = vec![1, 2, 3, 4];
        //El vector y su contenido es limpiado al salir de este ámbito.
    }

    //Acceso por índice.
    let v = vec![1, 2, 3, 4, 5];
    let acceso: &i32 = &v[2];
    println!("{}", acceso);

    //Acceso por el método get.
    //get devuelve Option<&T>.
    let v_index = 2;
    match v.get(v_index) {
        Some(_) => println!("El elemento ha sido encontrado: {}", v_index),
        None => println!("No ha sido posible encontrar el elemento: {}", v_index),
    }

    //Manejo de errores:
    //El siguiente código produce panic.
    //let no_existe = &v[100];
    
    //El método get no produce panic.
    match v.get(100) {
        Some(_) => println!("Esto nunca será alcanzado."),
        None => println!("get devuelve None al tratar de utilizar un índice fuera de rango."),
    }

    //Ownership también es aplicado en vectores.
    //let mut s = vec![1, 2, 3];
    //let reference: &i32 = &s[1];
    //s.push(4); //Problemas.

    //Iteraciones:
    let v = vec![554, 323, 76];
    //&v: No quiero mover ownership.
    for i in &v {
        print!("{} ", i);
    }
    print!("\n");
    
    let mut v = vec![554, 323, 76];
    for i in &mut v {
        *i *= 2;
        print!("{} ", i);
    }
    print!("\n");

    enum SpreadsheetCell {
        Int(i32),
        Float(f64),
        Text(String),
    }

    let row = vec![
        SpreadsheetCell::Int(10),
        SpreadsheetCell::Float(6.33),
        SpreadsheetCell::Text(String::from("Hello.")),
    ];

    //Push & Pop:
    let mut test = Vec::new();
    test.push(1);
    test.push(2);
    test.push(3);

    //pop remueve el último elemento y lo retorna como Option<T>.
    if let Some(i) = test.pop() {
        println!("{}", i);
    }

    for i in test {
        println!("{}", i);
    }

}
