Práctica de **flujo de control** basado en el [tercer capítulo de The Rust Programming Book](https://doc.rust-lang.org/book/ch03-05-control-flow.html), que abarca:
* Uso de condicionales: if, else if, else, if con let (declaración condicional).
* Uso de ciclos: loop, break (adicionalmente para retornar), while, for.
