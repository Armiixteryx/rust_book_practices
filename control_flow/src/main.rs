fn main() {
    let condicion = true;

    let numero = if condicion {
    	5
    } else {
    	6
    };

    //let numero = if condicion {
    //	5
    //} else {
    //	"six"
    //};

    println!("numero: {}", numero);


    loop {
    	println!("again");
    	break;
    }

    let mut numero = 3;

    while numero != 0 {
    	println!("{}!", numero);
    	numero = numero - 1;
    }

    let a = [10, 20, 30, 40, 50];

    for element in a.iter() {
    	println!("The value is: {}", element);
    }

    println!("Countdown:");

    for number in (1..4).rev() {
    	println!("{}!", number);
    }
}
