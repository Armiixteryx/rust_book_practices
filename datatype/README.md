Práctica de **tipos de datos** básicos en Rust, del [tercer capítulo de The Rust Programming Language](https://doc.rust-lang.org/book/ch03-02-data-types.html), que contiene:
* Anotaciones de tipo de dato.
* Tipos escalares: enteros, de punto flotante, booleano, caracter.
* Operaciones matemáticas con tipos escalares.
* Tipos compuestos: tuplas, array.
