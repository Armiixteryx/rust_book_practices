fn main() {

	//-------------------------- ANOTACIONES ------------------------//
	         //u32 es una anotación que indica el tipo de dato.
    let guess: u32 = "42".parse().expect("No es un número.");
    println!("guess: {:?}", guess);
    println!("\n");
    //---------------------------------------------------------------//

    //---------------------------- ENTEROS --------------------------//
    println!("ENTEROS.");

    let int8: i8 = 55;                         //Decimal.
    let uint8: u8 = 0x37;                      //Hexadecimal.
    let byte: u8 = b'Z';                       //Byte.
    let int16: i16 = 0o67;                     //Octal.
    let uint16: u16 = 0b0011_0111;             //Binario.
    let int32 = -1_884_643_434;                //Decorador visual.
    let uint32: u32 = 4134967296;              //Sin decorador visual.
    let int64: i64 = 8_231_884_643_434;
    let uint64: u64 = 545_665_523_887_323_777;

    println!("8 bits");
    println!("Con signo: {}", int8);
    println!("Sin signo: {}", uint8);
    println!("Byte: {}", byte);

    println!("16 bits");
    println!("Con signo: {}", int16);
    println!("Sin signo: {}", uint16);

    println!("32 bits");
    println!("Con signo: {}", int32);
    println!("Sin signo: {}", uint32);

    println!("64 bits");
    println!("Con signo: {}", int64);
    println!("Sin signo: {}", uint64);
    println!("\n");
    //---------------------------------------------------------------//


    //------------------------ PUNTO FLOTANTE -----------------------//
    println!("PUNTO FLOTANTE.");

    let float32: f32 = 144.22;
    let float64 = 155.32553;

    println!("Precisión simple: {}", float32);
    println!("Precisión doble: {}", float64);
    println!("\n");
    //---------------------------------------------------------------//


    //------------------- OPERACIONES MATEMÁTICAS -------------------//
    println!("OPERACIONES MATEMÁTICAS.");

    let adicion = uint64 + 3;
    let substraccion = int16 - 86;
    let multiplicacion = float64 * float32;
    let division = float64 / float32;

    println!("Adición: {}", adicion);
    println!("Substracción: {}", substraccion);
    println!("Multiplicación: {}", multiplicacion);
    println!("División: {}", division);
    println!("\n");
    //---------------------------------------------------------------//


    //-------------------------- BOOLEANO ---------------------------//
    println!("TIPO BOOLEANO.");

    let t = true;
    let f: bool = false; //Investigar el porqué de ser explícito.

    println!("Verdadero: {}", t);
    println!("Falso: {}", f);
    println!("\n");
    //---------------------------------------------------------------//


    //-------------------------- CARACTER ---------------------------//
    println!("CARACTER.");

    let c = 'z';
    let z = 'ℤ';
    let heart_eyed_cat = '😻';

    println!("Caracter 1: {}", c);
    println!("Caracter 2: {}", z);
    println!("Caracter 3: {}", heart_eyed_cat);
    println!("\n");
    //---------------------------------------------------------------//

    //--------------------------- TUPLAS ----------------------------//
    println!("TUPLAS.");

    //Declaración:
    let tupla: (u64, i8, char, bool, f32) = (4425778, 89, 'ł', true, 96.4556);

    //Acceso: Desestructuración por patrones:
    let (a,b,c,d,e) = tupla;
    println!("Acceso por desestructuración.");
    println!("Primer elemento: {}", a);
    println!("Segundo elemento: {}", b);
    println!("Tercer elemento: {}", c);
    println!("Cuarto elemento: {}", d);
    println!("Quinto elemento: {}", e);

    //Acceso: Directo:
    println!("Acceso directo.");
    println!("Primer elemento: {}", tupla.0);
    println!("Segundo elemento: {}", tupla.1);
    println!("Tercer elemento: {}", tupla.2);
    println!("Cuarto elemento: {}", tupla.3);
    println!("Quinto elemento: {}", tupla.4);
    //---------------------------------------------------------------//

    //---------------------------- ARRAY ----------------------------//
    println!("VECTOR.");

    //Declaración:
    let array = [4, -2, 8, -4, 10, -5];

    //Acceso a un array:
    println!("Primer elemento: {}", array[0]);
    println!("Segundo elemento: {}", array[1]);
    println!("Tercer elemento: {}", array[2]);
    println!("Cuarto elemento: {}", array[3]);
    println!("Quinto elemento: {}", array[4]);
    println!("Sexto elemento: {}", array[5]);

    //Acceso a elementos inválidos:
    let index = 6;
    let invalido = array[index];
}
