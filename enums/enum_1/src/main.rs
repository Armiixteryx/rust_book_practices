fn main() {
    enum IpAddrKind {
        V4(u8, u8, u8, u8),
        V6(String),
    }

    let four = IpAddrKind::V4(127, 0, 0, 1);
    let six = IpAddrKind::V6(String::from("::1"));

    enum Message {
        Quit,
        Move {x: i32, y: i32},
        Write(String),
        ChangeColor(i32,i32,i32),
    }

    impl Message {
        fn call(&self) {
        }
    }

    let m = Message::Write(String::from("Hello"));
    m.call();

    let some_number = Some(5);
    let some_string = Some(String::from("Hello"));
    let absent_number: Option<i32> = None;
}
