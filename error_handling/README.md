Práctica de **manejo de errores** del [noveno capítulo de The Rust Programming Language](https://doc.rust-lang.org/book/ch09-00-error-handling.html), que contiene:
* Uso de panic! (errores sin recuperación).
* Uso del tipo Result (enum que, con match, se puede verificar su estado y actuar en consecuencia).
