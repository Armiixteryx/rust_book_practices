use std::fs::File;

fn main() {
    let f = File::open("hello.txt");

    let f = match f {
        Ok(file) => {
            println!("{:?}", file);
            file
            },
        Err(error) => {
            panic!("Fallo al leer el archivo: {:?}", error)
        },
    };
}
