fn main() {
    println!("Hello, world!");
    otra_funcion(); //Función sin parámetros.
    funcion_parametrica(10);
    otra_funcion_parametrica(false, '🤔');
    expresiones();
    let x = cinco();
    println!("x: {}", x);
    let x = suma_cinco(x);
    println!("x: {}", x);
}

fn otra_funcion() {
	println!("Otra función.");
}

fn funcion_parametrica(x: i32) {
	println!("x: {}", x);
}

fn otra_funcion_parametrica(x: bool, y: char) {
	println!("x: {} ; y: {}", x, y);
}

fn expresiones() {
	let y = {
		let x = 3;
		x+3
	};

	let z = {
		let x = y * 27;
		x * 3
	};
	println!("y: {} ; z: {}", y, z);
}

fn cinco() -> i32 {
	5
}

fn suma_cinco(x: i32) -> i32 {
	x + cinco()
}