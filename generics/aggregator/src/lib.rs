pub trait Summary {
    //fn summarize(&self) -> String;

    fn summarize_author(&self) -> String;

    fn summarize(&self) -> String {
        format!("(Read more from {}...)", self.summarize_author())
    }
}

pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}

impl Summary for NewsArticle {
    fn summarize_author(&self) -> String {
        format!("{}", self.author)
    }
    //fn summarize(&self) -> String {
    //    format!("{}, by {} ({})", self.headline, self.author, self.location)
    //}
}

pub struct Tweet {
    pub username: String,
    pub content: String,
    pub reply: bool,
    pub retweet: bool,
}

impl Summary for Tweet {
    fn summarize_author(&self) -> String {
        format!("@{}", self.username)
    }

    fn summarize(&self) -> String {
        format!("{}: {}", self.summarize_author(), self.content)
    }
}

//Trait bounds:
pub fn notify<T: Summary>(item: &T) {
    println!("Breaking news! {}", item.summarize())
}

//Trait como argumento
pub fn notify_2(item: &impl Summary) {
    println!("Breaking news! {}", item.summarize());
}

pub fn notify_all<T: Summary, U: Summary>(item_1: &T, item_2: &U) {
    println!("Breaking news!\nTweet: {}\nArticle: {}",
    item_1.summarize(), item_2.summarize());
}

pub fn return_tweet() -> impl Summary {
    Tweet {
        username: String::from("someone"),
        content: String::from("something"),
        reply: false,
        retweet: false,
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
