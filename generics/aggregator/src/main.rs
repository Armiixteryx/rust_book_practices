extern crate aggregator;
use aggregator::{
    Summary,
    Tweet,
    NewsArticle,
};

fn main() {
    let tweet = Tweet {
        username: String::from("horse_power"),
        content: String::from("I thought it is new."),
        reply: false,
        retweet: false,
    };

    println!("1 new tweet: {} ", tweet.summarize());

    let article = NewsArticle {
        headline: String::from("Penguins win the Stanley Cup Championship!"),
        location: String::from("Pittsburgh, PA, USA"),
        author: String::from("Iceburgh"),
        content: String::from("The Pittsburgh Penguins once again are the best
        hockey team in the NHL."),
    };

    println!("New article found: {}", article.summarize());

    //Trait como argumento y Trait bounds:
    aggregator::notify(&tweet);
    aggregator::notify(&article);
    aggregator::notify_all(&tweet, &article);

    //Retorno de trait:
    let tweet_2 = aggregator::return_tweet();
}