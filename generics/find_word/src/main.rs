fn main() {
    let text = "hello world";
    let word = find_word(text);
    println!("{}", word);

    let text = "only_one_word";
    let word = find_word(text);
    println!("{}", word);
}

fn find_word(s: &str) -> &str {
	let text_bytes = s.as_bytes();

	for (i, &item) in text_bytes.iter().enumerate() {
		if item == b' ' {
			return &s[..i];
		}
	}

	&s[..]
}