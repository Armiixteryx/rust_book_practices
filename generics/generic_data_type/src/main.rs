fn main() {
    //En Rust, los tipos genéricos no afectan el rendimiento.
    //Esto es así porque el compilador aplica "monomorforización".
    //(crea código para cada tipo de dato a usar en el genérico).

    //Estructuras:
    //En este caso, x e y tienen que ser del mismo tipo.
    struct Point<T> {
        x: T,
        y: T,
    }

    let integer = Point {x: 5, y: 10};
    let float = Point {x: 6.0, y: 12.2};
    //let error = Point {x: 9.1, y: 3};
    //Descomentar arriba para ver el error.

    //En el siguiente caso, x e y NO tienen que ser del mismo tipo.
    struct OtherPoint<T, U> {
        x: T,
        y: U,
    }

    let mix = OtherPoint {x: 3.2, y: 5}; //VÁLIDO

    //Métodos:
    impl<T> Point<T> {
        fn x(&self) -> &T {
            &self.x
        }
    }
    let p = Point {x: 5, y: 10};
    println!("p.x: {}", p.x());

    //Implementación específica de un tipo genérico:
    impl Point<f32> {
        fn distance_from_origin(&self) -> f32 {
            (self.x.powi(2) + self.y.powi(2)).sqrt()
        }
    }

    let p = Point {x: 9.2, y: 4.76};
    println!("Distance from origin: {}", p.distance_from_origin());

    //Los métodos no necesitan tener los mismos parámetros genéricos
    //que las estructuras en que se basan.

    impl<T, U> OtherPoint<T, U> {
        //T, U son relevantes para toda la estructura.
        fn mixup<V, W>(self, other: OtherPoint<V, W>) -> OtherPoint<T, W> {
            //V, W son relevantes solamente para el método.
            OtherPoint {
                x: self.x,
                y: other.y,
            }
        }
    }

    let p1 = OtherPoint {x: 16, y: 43.5};
    let p2 = OtherPoint {x: 'c', y: "abc"};
    let p3 = p1.mixup(p2);
    println!("p3.x: {} | p3.y: {}", p3.x, p3.y);

    //Enumeraciones:
    enum ExampleOption<T> {
        Some(T),
        None,
    }

    let example_option = ExampleOption::Some(String::from("abc"));
    match example_option {
        ExampleOption::Some(string) => {
            println!("ExampleOption: {}", string);
        },
        ExampleOption::None => {
            println!("None found.");
        },
    }

    enum ExampleResult<T, E> {
        Ok(T),
        Err(E),
    }
}
