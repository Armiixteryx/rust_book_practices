fn main() {
    let number_list = vec![35, 21, 63, 449, 1234, 435, 439, 948, 854];
    let result = largest(&number_list);
    println!("The largest number is: {}", result);
    println!("{:?}", result);
    
    let char_list = vec!['c', 'd', 'f', 'm', 'a', 'b', 'r'];
    let result = largest_reference(&char_list);
    println!("The largest char is: {}", result);
    println!("{:?}", result);
}

fn largest<T: PartialOrd + Copy>(list: &[T]) -> T {
    let mut largest = list[0];

    for &i in list.iter() {
        if i > largest {
            largest = i;
        }
    }

    largest
}

fn largest_reference<T: PartialOrd>(list: &[T]) -> &T {
    let mut largest = &list[0];

    for item in list.iter() {
        if item > largest {
            largest = item;
        }
    }

    largest
}