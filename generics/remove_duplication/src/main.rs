fn main() {
    let number_list = vec![1, 5, 8, 25, 100, 96, 35, 120];
    let mut largest = number_list[0];
    for i in number_list {
        if i > largest {
            largest = i;
        }
    }
    println!("The largest number is: {}", largest);

    let number_list = vec![600, 200, 1800, 32, 43, 664, 85];
    let mut largest = number_list[0];
    for i in number_list {
        if i > largest {
            largest = i;
        }
    }
    println!("The largest number is: {}", largest);

    //Como el algoritmo es igual para cualquier vector de enteros,
    //es mejor abstraer el algoritmo a una función.

    let number_list = vec![35, 21, 63, 449, 1234, 435, 439, 948, 854];
    let result = largest_number(&number_list);
    println!("The largest number is: {}", result);
}

fn largest_number(list: &[i32]) -> i32 {
    let mut largest = list[0];

    for &i in list.iter() {
        if i > largest {
            largest = i;
        }
    }

    largest
}