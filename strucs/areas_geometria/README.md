Práctica de **estructuras**, basado en en el [quinto capítulo de The Rust Programming Language](https://doc.rust-lang.org/book/ch05-00-structs.html<Paste>). Contiene:
* Declaración de estructuras.
* Implementación de funciones asociadas a estructuras.
* Implementación de métodos (funciones asociadas a estructuras que tienen a `self` como primer parámetro).
