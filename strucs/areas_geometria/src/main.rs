struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }
    
    fn can_hold(&self, other: &Rectangle) -> bool {
        other.width < self.width && other.width < self.width
    }
}

impl Rectangle {
    fn square(size: u32) -> Rectangle {
        Rectangle {width: size, height: size}
    }

    fn generic (width: u32, height: u32) -> Rectangle {
        Rectangle {width, height}
    }
}

fn main() {
    let square = Rectangle::square(10);
    let rectangle = Rectangle::generic(30, 25);

    println!("square area is: {}", square.area());
    println!("rectangle area is: {}", rectangle.area());

    println!("Can square hold rectangle? {}", square.can_hold(&rectangle));
    println!("Can rectangle hold square? {}", rectangle.can_hold(&square));
}
