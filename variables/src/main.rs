fn main() {
    let mut x = 5;
    println!("El valor de x es: {}", x);
    x = 6;
    println!("El valor de x es: {}", x);


    //Declaración de una constante:
    const MAX_POINTS: u32 = 100_000 + 1;
    println!("MAX_POINTS: {}", MAX_POINTS);


    //Shadowing:
    let a = 5; //Compilador: La variable no ha sido usada.
    let a = 10;
    println!("El valor de a es: {}", a); //10
    let a = a * 2;
    println!("Ahora el valor de a es: {}", a); //20
    let a = a + 3;
    println!("Ahora el valor de a es: {}", a); //23
    let a = "Soy una cadena";
    println!("Ahora el valor de a es: {}", a); //Una cadena.

    //Shadowing con mutables:
    let mut b = "Soy otra cadena"; //Compilador: No necesita ser mutable.
    println!("El valor de b es: {}", b);
    //b = 3; //Esto no funcionará (asignación de distintos tipos).
    let b = 3; //Esto sí.
    println!("El nuevo valor de b es: {}", b);

    //Lo siguiente no es shadowing:
    //let mut spaces = "   ";
    //spaces = spaces.len();

}
